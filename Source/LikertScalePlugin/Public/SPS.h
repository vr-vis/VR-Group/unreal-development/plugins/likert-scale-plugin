// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "LikertScale.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SPS.generated.h"

/**
 * 
 */
UCLASS()
class LIKERTSCALEPLUGIN_API ASPS : public ALikertScale
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ASPS();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	//Called when an instance of this class is placed (in editor) or spawned.
	virtual void OnConstruction(const FTransform& Transform) override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
private:
	void AddQuestion(FText QuestionText);
	bool isQuestionAdded;
};
