// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Events/DisplayClusterEventWrapper.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "AnswerWidget.generated.h"

/**
 * 
 */
class UAnswerWidget;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnClickEvent, UAnswerWidget*, CheckBox);

UCLASS()
class LIKERTSCALEPLUGIN_API UAnswerWidget : public UUserWidget
{
	GENERATED_BODY()
public:
  void NativeConstruct() override;
  void NativeDestruct() override;
  void SynchronizeProperties() override;

  void SetValue(uint16 num);
  int16 GetValue() const;
  bool IsSelected() const;

  UFUNCTION()
  void Deselect();

  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
  UButton* CheckButton;
  UPROPERTY(BlueprintReadWrite, meta = (BindWidgetOptional))
  UTextBlock* ValueBlock;
  UPROPERTY(BlueprintAssignable, Category = "Event")
  FOnClickEvent FOnClicked;
  UPROPERTY(BlueprintReadWrite, Category = "AnswerWidget")
  bool bDeselecting = false;

private:
  UFUNCTION()
  void OnClicked();


  void ClusterSyncOnClicked();
  DECLARE_DISPLAY_CLUSTER_EVENT(UAnswerWidget, ClusterSyncOnClicked);

  bool Selected = false;
  int16 Value = 5;
};
