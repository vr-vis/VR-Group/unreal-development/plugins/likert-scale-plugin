// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Widgets/AnswerWidget.h"
#include "Widgets/QuestionWidget.h"
#include "Util/LikertScaleEvent.h"
#include "Util/Question.h"

#include "Events/DisplayClusterEventWrapper.h"

#include <vector>

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Components/UniformGridPanel.h"
#include "MainWidget.generated.h"

//class UQuestionWidget;
/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInteractionEvent, ULikertScaleEvent*, Event);

UCLASS()
class LIKERTSCALEPLUGIN_API UMainWidget : public UUserWidget
{
	GENERATED_BODY()
public:
  UMainWidget(const FObjectInitializer& ObjectInitializer);
  void NativeConstruct() override;
  void NativeDestruct() override;
  void SynchronizeProperties() override;

  TArray<int> GetAnswers() const;

  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
  UUniformGridPanel* QuestionGrid;
  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
  UButton* SubmitButton;
  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
  UTextBlock* SubmitText;
  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
  UButton* PreviousButton;
  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
  UButton* CustomButton;
  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
  UTextBlock* CustomButtonText;
  UPROPERTY(EditAnywhere, Category = LikertScale)
  UAnswerWidget* CustomAnswerWidget = nullptr;


  UPROPERTY(BlueprintAssignable, Category = "Event")
  FOnInteractionEvent FOnInteraction;

  UFUNCTION(BlueprintCallable, Category = "LikertScale")
  void OnSubmit();
  UFUNCTION(BlueprintCallable, Category = "LikertScale")
  void OnPrevious();
  UFUNCTION(BlueprintCallable, Category = "LikertScale")
  void OnCustom();

  void ClusterSyncOnSubmit();
  DECLARE_DISPLAY_CLUSTER_EVENT(UMainWidget, ClusterSyncOnSubmit);
  void ClusterSyncOnPrevious();
  DECLARE_DISPLAY_CLUSTER_EVENT(UMainWidget, ClusterSyncOnPrevious);
  void ClusterSyncOnCustom();
  DECLARE_DISPLAY_CLUSTER_EVENT(UMainWidget, ClusterSyncOnCustom);

  UPROPERTY()
  TArray<UQuestion*> Questions;
  bool bMultiLine = false;
  bool bWithPreviousButton = false;
  bool WithCustomButton = false;
  
  bool AllQuestionsAnswered() const;
private:
  UFUNCTION()
  void OnQuestionAnswered(UQuestionWidget* Question);

  void NextQuestion();
  void PreviousQuestion();
  void ToggleCustomButton();
  ULikertScaleEvent* CreateNewEvent(EEventType Type) const;

  UPROPERTY()
  FString AnswerWidgetPath = FString("/LikertScalePlugin/WidgetBlueprints/DefaultAnswerWidget.DefaultAnswerWidget_C");
  TSubclassOf<UQuestionWidget> QuestionClass = nullptr;
  std::vector<UQuestionWidget*> QuestionsVec;
  int16 CurrentQuestionIndex = 0;
};
