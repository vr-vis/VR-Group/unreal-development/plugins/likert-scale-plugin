// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Widgets/AnswerWidget.h"

#include "Events/DisplayClusterEventWrapper.h"

#include <vector>
#include <math.h>

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/MultiLineEditableTextBox.h"
#include "Components/UniformGridPanel.h"
#include "QuestionWidget.generated.h"



/**
 * 
 */
class UQuestionWidget;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnQuestionAnsweredEvent, UQuestionWidget*, Question);

UCLASS()
class LIKERTSCALEPLUGIN_API UQuestionWidget : public UUserWidget
{
	GENERATED_BODY()
 public:
  UQuestionWidget(const FObjectInitializer& ObjectInitializer);
  void NativeConstruct() override;
  void NativeDestruct() override;

  void SynchronizeProperties() override;

  void SetHighestAnswer(int16 Num);
  int16 GetHighestAnswer() const;
  void SetLowestAnswer(int16 Answer);
  int16 GetLowestAnswers() const;
  int16 GetAnswer() const;
  bool IsAnswered() const;

  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
  UMultiLineEditableTextBox* QuestionBox;
  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
  UMultiLineEditableTextBox* AgreeBox;
  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
  UMultiLineEditableTextBox* DisagreeBox;
  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
  UButton* NoAnswerButton;
  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* NoAnswerText;
  UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
  UUniformGridPanel* AnswerGrid;
  UPROPERTY(EditAnywhere, Category = LikertScale)
  UAnswerWidget* CustomAnswerWidget = nullptr;
  UPROPERTY(BlueprintAssignable, Category = "Event")
  FOnQuestionAnsweredEvent FOnQuestionAnswered;

  UPROPERTY(BlueprintReadWrite, Category = "AnswerWidget")
  bool DeselectingNoAnswerButton = false;


 private:
  UFUNCTION()
  void OnAnswerClicked(UAnswerWidget* Answer);

  UFUNCTION()
  void OnNoAnswer();

  void ClusterSyncOnNoAnswer();
  DECLARE_DISPLAY_CLUSTER_EVENT(UQuestionWidget, ClusterSyncOnNoAnswer);

  void DeselectNoAnswerButton();

  UPROPERTY()
  UAnswerWidget* ClickedAnswer = nullptr;
  bool bAnswered = false;
  std::vector<UAnswerWidget*> Answers;
  int16 AnswerValue = NAN;
  int16 HighestAnswer = 5;
  int16 LowestAnswer = 0;
  TSubclassOf<UAnswerWidget> AnswerClass = nullptr;
  bool bNoAnswer = false;
};
