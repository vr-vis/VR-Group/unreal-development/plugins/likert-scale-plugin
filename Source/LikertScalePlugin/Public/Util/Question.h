// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Question.generated.h"

/**
 * 
 */
UCLASS(EditInlineNew)
class LIKERTSCALEPLUGIN_API UQuestion : public UObject
{
	GENERATED_BODY()
public:
	UQuestion();
  UPROPERTY(EditAnywhere, Category = LikertScale, meta = (MultiLine = "true"))
  FText Question = FText::FromString("Example Question");
  UPROPERTY(EditAnywhere, Category = LikertScale, meta = (MultiLine = "true"))
  FText AgreeText = FText::FromString("Strongly\nAgree");
  UPROPERTY(EditAnywhere, Category = LikertScale, meta = (MultiLine = "true"))
  FText DisagreeText = FText::FromString("Strongly\nDisagree");
  UPROPERTY(EditAnywhere, Category = LikertScale)
  int16 HighestAnswer = 5;
  UPROPERTY(EditAnywhere, Category = LikertScale)
  int16 LowestAnswer = 0;
  UPROPERTY(EditAnywhere, Category = LikertScale)
  bool bEnableCustomButton = false;
  UPROPERTY(EditAnywhere, Category = LikertScale, meta = (EditCondition = "EnableCustomButton"))
  FText CustomButtonText = FText::FromString("Custom Button");
  UPROPERTY(EditAnywhere, Category = LikertScale)
	bool bShowQuestion = true;
  UPROPERTY(EditAnywhere, Category = LikertScale)
	bool bEnableNoAnswer = true;
};
