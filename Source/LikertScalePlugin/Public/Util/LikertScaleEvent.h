// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Util/Question.h"

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "LikertScaleEvent.generated.h"

/**
 * 
 */
UENUM()
enum class EEventType : uint8
{
  AnswerSelected,
  Submit,
  Previous,
  Next,
  CustomButton
};

UCLASS()
class LIKERTSCALEPLUGIN_API ULikertScaleEvent : public UObject
{
	GENERATED_BODY()
public:
  ULikertScaleEvent() = default;
  ~ULikertScaleEvent() = default;

  UPROPERTY(BlueprintReadOnly)
  EEventType Type;

  // Holds all questions
  UPROPERTY(BlueprintReadOnly)
  TArray<UQuestion*> Questions;

  // Holds all currently given answers
  UPROPERTY(BlueprintReadOnly)
  TArray<int> Answers;

  // If EEventType is "AnswerSelected" this holds the index of the just answered question
  UPROPERTY(BlueprintReadOnly)
  int TriggeredByQuestionIndex;
};
