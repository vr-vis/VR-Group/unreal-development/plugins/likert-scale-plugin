// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <vector>

#include "Util/Question.h"
#include "Widgets/MainWidget.h"

//#include "DisplayClusterEventWrapper.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/WidgetComponent.h"

#include "LikertScale.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLikertScaleEvent, ULikertScaleEvent*, Event);

UCLASS()
class LIKERTSCALEPLUGIN_API ALikertScale : public AActor
{
	GENERATED_BODY()
	
public:	
  // Sets default values for this actor's properties
  ALikertScale();

protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;
  virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
  // Called every frame
  virtual void Tick(float DeltaTime) override;
  virtual bool ShouldTickIfViewportsOnly() const override;

  void Init();

  UPROPERTY(BlueprintAssignable, Category = "Event")
  FOnLikertScaleEvent FLikertScaleEvent;

  UPROPERTY(EditAnywhere, Category = "Likert Scale | General")
  bool bWithPreviousButton = false;
  UPROPERTY(EditAnywhere, Category = "Likert Scale | General")
  bool bMultiLine = false;

  UPROPERTY(EditAnywhere, Category = "Likert Scale | Question Settings", meta = (MultiLine = "true"))
  FString AgreeText = FString("Strongly\nAgree");
  UPROPERTY(EditAnywhere, Category = "Likert Scale | Question Settings", meta = (MultiLine = "true"))
  FString DisagreeText = FString("Strongly\nDisagree");
  UPROPERTY(EditAnywhere, Category = "Likert Scale | Question Settings")
  int16 HighestAnswer = 5;
  UPROPERTY(EditAnywhere, Category = "Likert Scale | Question Settings")
  int16 LowestAnswer = 0;
  UFUNCTION(BlueprintCallable, CallInEditor, Category = "Likert Scale | Question Settings")
  void SetForAllQuestions();

  UPROPERTY(EditAnywhere, Category = "Likert Scale | Design")
  UMainWidget* CustomMainWidget = nullptr;
  UPROPERTY(EditAnywhere, Category = "Likert Scale | Design")
  UAnswerWidget* CustomAnswerWidget = nullptr;
  UPROPERTY(EditAnywhere, Category = "Likert Scale | Design")
  UQuestionWidget* CustomQuestionWidget = nullptr;

  UFUNCTION(BlueprintCallable, CallInEditor, Category = "Likert Scale | Question")
  void AddQuestion();
  UFUNCTION(BlueprintCallable, CallInEditor, Category = "Likert Scale | Question")
  void RemoveLastQuestion();
  UPROPERTY(EditAnywhere, Instanced, EditFixedSize, Category = "Likert Scale | Question")
  TArray<UQuestion*> Questions;

 

  UPROPERTY(EditAnywhere)
  UMainWidget* MainWidget = nullptr;
  UPROPERTY(EditAnywhere)
  UWidgetComponent* MainWidgetComponent;

private:
#if WITH_EDITOR
  virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

  UFUNCTION()
  void OnInteraction(ULikertScaleEvent* Event);

  UFUNCTION()
  void EmitLikertScaleDelegate();
  DECLARE_DISPLAY_CLUSTER_EVENT(ALikertScale, EmitLikertScaleDelegate);

  ULikertScaleEvent* LastEvent = nullptr;

  int Test = 0;
};
