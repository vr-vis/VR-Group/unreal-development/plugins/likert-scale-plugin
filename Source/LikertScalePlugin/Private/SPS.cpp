// Fill out your copyright notice in the Description page of Project Settings.


#include "SPS.h"


// Sets default values
ASPS::ASPS()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	isQuestionAdded = false;
}

// Called when the game starts or when spawned
void ASPS::BeginPlay()
{
	Super::BeginPlay();

}

void ASPS::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	
	//Empties questions otherwhise the questions are created multiple times
	Questions.Empty();

	AddQuestion(FText::FromString("I perceive that I am in the presence of another person in the room with me."));


	AddQuestion(FText::FromString("I feel that the person is watching me and is aware of my presence."));

	AddQuestion(FText::FromString("The thought that the person is not a real person crosses my mind often."));

	AddQuestion(FText::FromString("The person appears to be sentient (conscious and alive) to me."));

	AddQuestion(FText::FromString("I perceive the person as being only a computerized image, not as a real person."));

}

// Called every frame
void ASPS::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASPS::AddQuestion(FText QuestionText) {
	isQuestionAdded = true;
	FString Name = "QuestionSPS";
	Name.AppendInt(Questions.Num());
	auto* Question = NewObject<UQuestion>(this, FName(*Name));
	Question->Question = QuestionText;
	Question->AgreeText = FText::FromString("Normal experience of\n being in a place");
	Question->DisagreeText = FText::FromString("Example Question");
	Question->HighestAnswer = 3;
	Question->LowestAnswer = -3;
	Questions.Add(Question);
}
