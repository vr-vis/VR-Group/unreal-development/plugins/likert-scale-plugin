// Fill out your copyright notice in the Description page of Project Settings.


#include "LikertScale.h"
#include "Components/WidgetComponent.h"
	#include "UObject/ConstructorHelpers.h"
#include "Engine/Engine.h"


ALikertScale::ALikertScale()
{
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;
  PrimaryActorTick.bStartWithTickEnabled = true;

  static ConstructorHelpers::FClassFinder<UUserWidget>ClassFinder(TEXT("/LikertScalePlugin/WidgetBlueprints/DefaultMainWidget.DefaultMainWidget_C"));
  MainWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("MainWidget");
  MainWidgetComponent->SetWidgetSpace(EWidgetSpace::World);
  MainWidgetComponent->SetWidgetClass(ClassFinder.Class);
  MainWidgetComponent->SetDrawSize(FVector2D(7680, 4320));
 
  RootComponent = MainWidgetComponent;
  Init();

}

// Called when the game starts or when spawned
void ALikertScale::BeginPlay()
{
  Super::BeginPlay();
  //EmitLikertScaleDelegateEvent.Attach(this);
  Init();
  if (MainWidget)
  {
    MainWidget->FOnInteraction.AddDynamic(this, &ALikertScale::OnInteraction);
    MainWidget->PreviousButton->SetVisibility(ESlateVisibility::Hidden);
    MainWidget->PreviousButton->SetIsEnabled(false);
  }
}

void ALikertScale::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	//EmitLikertScaleDelegateEvent.Detach();
	Super::EndPlay(EndPlayReason);
}


// Called every frame (also within the editor)
void ALikertScale::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);
  Init();
}

bool ALikertScale::ShouldTickIfViewportsOnly() const
{
  return true;
}

void ALikertScale::Init()
{
  MainWidget = dynamic_cast<UMainWidget*>(MainWidgetComponent->GetUserWidgetObject());
  if (CustomMainWidget)
  {
    MainWidgetComponent->SetWidgetClass(CustomMainWidget->GetClass());
    RootComponent = MainWidgetComponent;
    MainWidget = dynamic_cast<UMainWidget*>(MainWidgetComponent->GetUserWidgetObject());
  }
  if (CustomAnswerWidget)
  {
    MainWidget->CustomAnswerWidget = CustomAnswerWidget;
  }

  if (MainWidget)
  {
    MainWidget->Questions = Questions;
    MainWidget->bMultiLine = bMultiLine;
	MainWidget->bWithPreviousButton = bWithPreviousButton;
  }
}

void ALikertScale::SetForAllQuestions()
{
  for (auto Question: Questions)
  {
    Question->HighestAnswer = HighestAnswer;
    Question->LowestAnswer = LowestAnswer;
    Question->AgreeText = FText::FromString(AgreeText);
    Question->DisagreeText = FText::FromString(DisagreeText);
  }
}

void ALikertScale::AddQuestion()
{
  FString Name = "Question";
  Name.AppendInt(Questions.Num());
  auto* Question = NewObject<UQuestion>(this, FName(*Name));
  Questions.Add(Question);
  Init();
}

void ALikertScale::RemoveLastQuestion()
{
  if (Questions.Num() > 0)
  {
    Questions.Pop();
    Init();
  }
}

#if WITH_EDITOR
void ALikertScale::PostEditChangeProperty(FPropertyChangedEvent & PropertyChangedEvent)
{
  Super::PostEditChangeProperty(PropertyChangedEvent);
  Init();
}
#endif

void ALikertScale::OnInteraction(ULikertScaleEvent* Event)
{
  LastEvent = Event;
  EmitLikertScaleDelegate();
  //EmitLikertScaleDelegateEvent.Send();
}

void ALikertScale::EmitLikertScaleDelegate()
{
  if (LastEvent != nullptr) {
  	/*if (LastEvent->Type == EEventType::Submit)
  	{
      GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Submit!"));
  	}
  	else if (LastEvent->Type == EEventType::Next)
  	{
  	  GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Next!"));
  	}
  	else if (LastEvent->Type == EEventType::Previous)
  	{
  	  GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Previous!"));
  	}
  	else if (LastEvent->Type == EEventType::AnswerSelected)
  	{
  	  Test++;
  	  GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Answer: %d, clicked!"), LastEvent->Answers[LastEvent->TriggeredByQuestionIndex]));
  	}
  	else if (LastEvent->Type == EEventType::CustomButton)
  	{
  	  GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Custom Button of Question: %d!"), LastEvent->TriggeredByQuestionIndex));
  	}*/
  	FLikertScaleEvent.Broadcast(LastEvent);
  }
}
