// Fill out your copyright notice in the Description page of Project Settings.


#include "SUS.h"

// Sets default values
ASUS::ASUS()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASUS::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASUS::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	//Empties questions otherwhise the questions are created multiple times
	Questions.Empty();

	AddQuestion(FText::FromString("Please rate your sense of being in the virtual environment, on a scale of 1 to 7, where 7\n"
									"represents your normal experience of being in a place."));

	AddQuestion(FText::FromString("To what extent were there times during the experience when the virtual environment was the\n"
									"reality for you?"));

	AddQuestion(FText::FromString("When you think back to the experience, do you think of the virtual environment more as\n"
									"images that you saw or more as somewhere that you visited?"));

	AddQuestion(FText::FromString("During the time of the experience, which was the strongest on the whole, your sense of being\n"
									"in the virtual environment or of being elsewhere?"));

	AddQuestion(FText::FromString("Consider your memory of being in the virtual environment. How similar in terms of the\n" 
									"structure of the memory is this to the structure of the memory of other places you have\n"
									"been today? By �structure of the memory� consider things like the extent to which you\n"
									"have a visual memory of the virtual environment, whether that memory is in colour,the extent\n"
									"to which the memory seems vivid or realistic, its size, location in your imagination,\n"
									"the extent to which it is panoramic in your imagination, and other such structural elements."));

	AddQuestion(FText::FromString("During the time of your experience, did you often think to yourself that you were actually\n"
									"in the virtual environment?"));
}

// Called every frame
void ASUS::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASUS::AddQuestion(FText QuestionText) {
	FString Name = "Question";
	Name.AppendInt(Questions.Num());
	auto* Question = NewObject<UQuestion>(this, UQuestion::StaticClass(), FName(*Name));
	Question->Question = QuestionText;
	Question->AgreeText = FText::FromString("Normal experience of\n being in a place");
	Question->DisagreeText = FText::FromString("Example Question");
	Question->HighestAnswer = 7;
	Question->LowestAnswer = 1;
	Questions.Add(Question);
}

