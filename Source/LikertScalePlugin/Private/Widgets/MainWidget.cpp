// Fill out your copyright notice in the Description page of Project Settings.


#include "Widgets/MainWidget.h"

#include "Components/UniformGridSlot.h"
#include "UObject/ConstructorHelpers.h"

UMainWidget::UMainWidget(const FObjectInitializer& ObjectInitializer) :
  Super(ObjectInitializer)
{
  static ConstructorHelpers::FClassFinder<UQuestionWidget> QuestionFinder(TEXT("/LikertScalePlugin/WidgetBlueprints/DefaultQuestionWidget.DefaultQuestionWidget_C"));
  if (QuestionFinder.Class)
  {
    QuestionClass = QuestionFinder.Class;
  }
}

void UMainWidget::NativeConstruct()
{
  Super::NativeConstruct();
  if (SubmitButton)
  {
	//ClusterSyncOnSubmitEvent.Attach(this);
    SubmitButton->SetIsEnabled(false);
    SubmitButton->OnClicked.AddDynamic(this, &UMainWidget::OnSubmit);
  }
  if (PreviousButton)
  {
	//ClusterSyncOnPreviousEvent.Attach(this);
    PreviousButton->OnClicked.AddDynamic(this, &UMainWidget::OnPrevious);
  }
  if (CustomButton)
  {
	//ClusterSyncOnCustomEvent.Attach(this);
    CustomButton->OnClicked.AddDynamic(this, &UMainWidget::OnCustom);
  }
}

void UMainWidget::NativeDestruct()
{
  /*if (SubmitButton)
  {
    ClusterSyncOnSubmitEvent.Detach();
  }
  if (PreviousButton)
  {
    ClusterSyncOnPreviousEvent.Detach();
  }
  if (CustomButton)
  {
    ClusterSyncOnCustomEvent.Detach();
  }*/
  Super::NativeDestruct();
}

void UMainWidget::SynchronizeProperties()
{
  Super::SynchronizeProperties();

  if (QuestionGrid)
  {
    if (CustomAnswerWidget)
    {
      QuestionClass = CustomAnswerWidget->GetClass();
    }
    QuestionsVec.clear();

 
    for (int32 q = 0; q < Questions.Num(); ++q) 
	{
      UQuestionWidget* Question = CreateWidget<UQuestionWidget>(GetWorld(), QuestionClass);
      if (Question)
      {
        Question->FOnQuestionAnswered.AddDynamic(this, &UMainWidget::OnQuestionAnswered);
        Question->QuestionBox->SetText(Questions[q]->Question);
        Question->AgreeBox->SetText(Questions[q]->AgreeText);
        Question->DisagreeBox->SetText(Questions[q]->DisagreeText);
        Question->SetHighestAnswer(Questions[q]->HighestAnswer);
        Question->SetLowestAnswer(Questions[q]->LowestAnswer);

        if(!Questions[q]->bShowQuestion)
        {
          Question->QuestionBox->SetVisibility(ESlateVisibility::Collapsed);
        }
        if (!Questions[q]->bEnableNoAnswer)
        {
          Question->NoAnswerButton->SetVisibility(ESlateVisibility::Collapsed);
          Question->NoAnswerText->SetVisibility(ESlateVisibility::Collapsed);
        }

        QuestionsVec.push_back(Question);

		if (bMultiLine)
		{
		  UUniformGridSlot* GridSlot = QuestionGrid->AddChildToUniformGrid(Question);
	      GridSlot->SetColumn(0);
		  GridSlot->SetRow(q);
		  GridSlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Center);
		  
		} 
		else
		{
		  if (q != 0) 
		  {
		    Question->SetVisibility(ESlateVisibility::Hidden);
		    Question->SetIsEnabled(false);
		  }
		  UUniformGridSlot* GridSlot = QuestionGrid->AddChildToUniformGrid(Question);
		  GridSlot->SetColumn(0);
		  GridSlot->SetRow(0);
		  
		}
      }
    }
	if (QuestionsVec.size() > 1 && !bMultiLine)
	{
	  SubmitText->SetText(FText::FromString("Next"));
	}
  }

  if (CustomButton && Questions.Num() > CurrentQuestionIndex)
  {
    if (Questions[CurrentQuestionIndex]->bEnableCustomButton)
    {
      CustomButton->SetIsEnabled(true);
      CustomButton->SetVisibility(ESlateVisibility::Visible);
	  CustomButtonText->SetText(Questions[CurrentQuestionIndex]->CustomButtonText);
    }
    else
    {
      CustomButton->SetIsEnabled(false);
      CustomButton->SetVisibility(ESlateVisibility::Hidden);
    } 
  }
}

void UMainWidget::OnSubmit()
{
  //ClusterSyncOnSubmitEvent.Send();
  ClusterSyncOnSubmit(); // Remove this when switching back to cluster events
}

void UMainWidget::OnPrevious()
{
  //ClusterSyncOnPreviousEvent.Send();
  ClusterSyncOnPrevious(); // Remove this when switching back to cluster events
}

void UMainWidget::OnCustom()
{
  //ClusterSyncOnCustomEvent.Send();
  ClusterSyncOnCustom();// Remove this when switching back to cluster events
}

void UMainWidget::OnQuestionAnswered(UQuestionWidget* Question)
{
  if (Question->IsAnswered())
  {
    if (AllQuestionsAnswered() || !bMultiLine)
    {
      SubmitButton->SetIsEnabled(true);
    }
    else
    {
      SubmitButton->SetIsEnabled(false);
    }
    ULikertScaleEvent* Event = CreateNewEvent(EEventType::AnswerSelected);
    FOnInteraction.Broadcast(Event);
  }
}

void UMainWidget::ClusterSyncOnSubmit()
{
  ULikertScaleEvent* Event = nullptr;
  if (AllQuestionsAnswered() && (bMultiLine || CurrentQuestionIndex == QuestionsVec.size() - 1))
  {
  	Event = CreateNewEvent(EEventType::Submit);
  }
  else
  {
  	Event = CreateNewEvent(EEventType::Next);
  	NextQuestion();
  }
  FOnInteraction.Broadcast(Event);
}

void UMainWidget::ClusterSyncOnPrevious() 
{
  ULikertScaleEvent* Event = CreateNewEvent(EEventType::Previous);
  PreviousQuestion();
  FOnInteraction.Broadcast(Event);
}

void UMainWidget::ClusterSyncOnCustom()
{
  ULikertScaleEvent* Event = CreateNewEvent(EEventType::CustomButton);
  FOnInteraction.Broadcast(Event);
}

bool UMainWidget::AllQuestionsAnswered() const
{
  bool AllAnswered = true;
  for (auto& Q : QuestionsVec)
  {
    if (!Q->IsAnswered())
    {
      AllAnswered = false;
      break;
    }
  }
  return AllAnswered;
}

TArray<int> UMainWidget::GetAnswers() const
{
  TArray<int> AnswerValues;
  for (auto& Q : QuestionsVec)
  {
	if (Q->IsAnswered())
	{
	  AnswerValues.Add(Q->GetAnswer());
	} 
	else
	{
	  AnswerValues.Add(NAN);
	}
  }
  return AnswerValues;
}

void UMainWidget::NextQuestion()
{
  QuestionsVec[CurrentQuestionIndex]->SetVisibility(ESlateVisibility::Hidden);
  QuestionsVec[CurrentQuestionIndex]->SetIsEnabled(false);

  CurrentQuestionIndex++;
  QuestionsVec[CurrentQuestionIndex]->SetVisibility(ESlateVisibility::Visible);
  QuestionsVec[CurrentQuestionIndex]->SetIsEnabled(true);

  SubmitButton->SetIsEnabled(QuestionsVec[CurrentQuestionIndex]->IsAnswered());
  ToggleCustomButton();
  if (CurrentQuestionIndex == QuestionsVec.size() - 1)
  {
    SubmitText->SetText(FText::FromString("Submit"));
  }
  if (PreviousButton)
  {
	if (bWithPreviousButton)
	{
	  PreviousButton->SetIsEnabled(true);
	  PreviousButton->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
	  PreviousButton->SetIsEnabled(false);
      PreviousButton->SetVisibility(ESlateVisibility::Hidden);
	}
  }
}

void UMainWidget::PreviousQuestion()
{
  QuestionsVec[CurrentQuestionIndex]->SetVisibility(ESlateVisibility::Hidden);
  QuestionsVec[CurrentQuestionIndex]->SetIsEnabled(false);

  CurrentQuestionIndex--;
  QuestionsVec[CurrentQuestionIndex]->SetVisibility(ESlateVisibility::Visible);
  QuestionsVec[CurrentQuestionIndex]->SetIsEnabled(true);

  SubmitButton->SetIsEnabled(true);
  ToggleCustomButton();

  SubmitText->SetText(FText::FromString("Next"));
  if (CurrentQuestionIndex == 0 && PreviousButton)
  {
	PreviousButton->SetIsEnabled(false);
	PreviousButton->SetVisibility(ESlateVisibility::Hidden);
  }
}

void UMainWidget::ToggleCustomButton() {
  if (CustomButton && Questions.Num() > CurrentQuestionIndex)
  {
  	if (Questions[CurrentQuestionIndex]->bEnableCustomButton)
  	{
  	  CustomButton->SetIsEnabled(true);
  	  CustomButton->SetVisibility(ESlateVisibility::Visible);
  	  CustomButtonText->SetText(Questions[CurrentQuestionIndex]->CustomButtonText);
  	}
  	else
  	{
  	  CustomButton->SetIsEnabled(false);
  	  CustomButton->SetVisibility(ESlateVisibility::Hidden);
  	}
  }
}

ULikertScaleEvent* UMainWidget::CreateNewEvent(EEventType Type) const
{
  ULikertScaleEvent* Event = NewObject<ULikertScaleEvent>();
  Event->TriggeredByQuestionIndex = CurrentQuestionIndex;
  Event->Questions = Questions;
  Event->Answers = GetAnswers();
  Event->Type = Type;
  return Event;
}
