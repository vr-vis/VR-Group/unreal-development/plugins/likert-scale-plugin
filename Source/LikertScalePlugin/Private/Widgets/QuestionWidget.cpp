// Fill out your copyright notice in the Description page of Project Settings.

#include "Widgets/QuestionWidget.h"

#include "UObject/ConstructorHelpers.h"
#include "Components/UniformGridSlot.h"
#include "Components/Spacer.h"
#include "Blueprint/WidgetTree.h"

UQuestionWidget::UQuestionWidget(const FObjectInitializer& ObjectInitializer) :
  Super(ObjectInitializer)
{
  static ConstructorHelpers::FClassFinder<UAnswerWidget> AnswerFinder(TEXT("/LikertScalePlugin/WidgetBlueprints/DefaultAnswerWidget.DefaultAnswerWidget_C"));
  if (AnswerFinder.Class)
  {
    AnswerClass = AnswerFinder.Class;
  }
}

void UQuestionWidget::NativeConstruct()
{
  Super::NativeConstruct();
  // ClusterSyncOnNoAnswerEvent.Attach(this);
  if (NoAnswerButton)
  {
	NoAnswerButton->SetBackgroundColor(FLinearColor::Gray);
    NoAnswerButton->OnClicked.AddDynamic(this, &UQuestionWidget::OnNoAnswer);
  }
}

void UQuestionWidget::NativeDestruct()
{
	// ClusterSyncOnNoAnswerEvent.Detach();
	Super::NativeDestruct();
}

void UQuestionWidget::SynchronizeProperties()
{
  Super::SynchronizeProperties();

  if (AnswerGrid)
  {
    if (CustomAnswerWidget)
    {
      AnswerClass = CustomAnswerWidget->GetClass();
    }

    int Column = 0;
	
	USpacer* Spacer1 = WidgetTree->ConstructWidget<USpacer>();
	Spacer1->SetSize(FVector2D(2, 1));
	UUniformGridSlot* SpacerSlot1 = AnswerGrid->AddChildToUniformGrid(Spacer1);
	SpacerSlot1->SetColumn(Column++);
	SpacerSlot1->SetRow(0);
    
	for (int32 i = LowestAnswer; i <= HighestAnswer; ++i)
    {
      UAnswerWidget* Answer = CreateWidget<UAnswerWidget>(GetWorld(), AnswerClass);
      if (Answer)
      {
        Answer->SetValue(i);
        Answer->FOnClicked.AddDynamic(this, &UQuestionWidget::OnAnswerClicked);
        Answers.push_back(Answer);
        UUniformGridSlot* GridSlot = AnswerGrid->AddChildToUniformGrid(Answer);
        GridSlot->SetColumn(Column++);
        GridSlot->SetRow(0);
		GridSlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Center);
      }
    }
	
	USpacer* Spacer = WidgetTree->ConstructWidget<USpacer>();
	Spacer->SetSize(FVector2D(2, 1));
	UUniformGridSlot* SpacerSlot = AnswerGrid->AddChildToUniformGrid(Spacer);
	SpacerSlot->SetColumn(Column++);
	SpacerSlot->SetRow(0);
  }
}

void UQuestionWidget::SetHighestAnswer(int16 Num)
{
  HighestAnswer = Num;
}

int16 UQuestionWidget::GetHighestAnswer() const
{
  return HighestAnswer;
}

void UQuestionWidget::SetLowestAnswer(int16 Answer)
{
  LowestAnswer = Answer;
}

int16 UQuestionWidget::GetLowestAnswers() const
{
  return LowestAnswer;
}

int16 UQuestionWidget::GetAnswer() const
{
  return AnswerValue;
}

bool UQuestionWidget::IsAnswered() const
{
  return bAnswered;
}

void UQuestionWidget::OnAnswerClicked(UAnswerWidget * Answer)
{
  // Handle deselect of not selected answers
  if (ClickedAnswer != nullptr && ClickedAnswer != Answer)
  {
    ClickedAnswer->Deselect();
  }
  if (bNoAnswer) {
    DeselectNoAnswerButton();
  }

  bAnswered = true;
  ClickedAnswer = Answer;
  AnswerValue = ClickedAnswer->GetValue();
  FOnQuestionAnswered.Broadcast(this);
}

void UQuestionWidget::OnNoAnswer()
{
  // ClusterSyncOnNoAnswerEvent.Send();
  ClusterSyncOnNoAnswer(); // Remove this when switching back to cluster events
}

void UQuestionWidget::ClusterSyncOnNoAnswer()
{
  if (DeselectingNoAnswerButton)
  {
  	bNoAnswer = false;
  	DeselectingNoAnswerButton = false;
	NoAnswerButton->SetBackgroundColor(FLinearColor::Gray);
  }
  else
  {
	NoAnswerButton->SetBackgroundColor(FLinearColor::Green);
  	if (ClickedAnswer != nullptr)
  	{
  	  ClickedAnswer->Deselect();
  	}
  	bAnswered = true;
  	bNoAnswer = true;
  	AnswerValue = NAN;
  	FOnQuestionAnswered.Broadcast(this);
  }
}

void UQuestionWidget::DeselectNoAnswerButton()
{
  if (bNoAnswer)
  {
    DeselectingNoAnswerButton = true;
    NoAnswerButton->OnClicked.Broadcast();
  }
}
