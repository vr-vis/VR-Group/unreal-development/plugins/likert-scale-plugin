// Fill out your copyright notice in the Description page of Project Settings.


#include "Widgets/AnswerWidget.h"

void UAnswerWidget::NativeConstruct()
{
  Super::NativeConstruct();
  //ClusterSyncOnClickedEvent.Attach(this);
  CheckButton->SetBackgroundColor(FLinearColor::Gray);
  CheckButton->OnClicked.AddDynamic(this, &UAnswerWidget::OnClicked);
}

void UAnswerWidget::NativeDestruct()
{
  //ClusterSyncOnClickedEvent.Detach();
  Super::NativeDestruct();
}

void UAnswerWidget::SynchronizeProperties()
{
  Super::SynchronizeProperties();

  if (ValueBlock)
  {
    ValueBlock->SetText(FText::FromString(FString::FromInt(Value)));
  }
}

void UAnswerWidget::SetValue(uint16 num)
{
  Value = num;
}

int16 UAnswerWidget::GetValue() const
{
  return Value;
}

void UAnswerWidget::OnClicked()
{
  //ClusterSyncOnClickedEvent.Send();
  ClusterSyncOnClicked(); // Remove this when switching back to cluster events
}

void UAnswerWidget::ClusterSyncOnClicked() 
{
  if (bDeselecting)
  {
  	bDeselecting = false;
  	CheckButton->SetBackgroundColor(FLinearColor::Gray);
  }
  else
  {
  	Selected = true;
  	CheckButton->SetBackgroundColor(FLinearColor::Green);
  	FOnClicked.Broadcast(this);
  }
}

bool UAnswerWidget::IsSelected() const
{
  return Selected;
}

void UAnswerWidget::Deselect()
{
  if (Selected)
  {
    bDeselecting = true;
    Selected = false;
    CheckButton->OnClicked.Broadcast();
  }
}


