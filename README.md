# Likert Scale Plugin
This Plugin contains a Likert Scale Template to create VR Likert Scales based on Unreals UMG Widgets.

## How To
Place the LikertScale cpp actor in your scene and modify it via the editor.
Add as many questions as you like.

In order to react on input to the Liker Scale , you need a reference to it and register to its Delegate as follows:

'
RefToLikertScale->FLikertScaleEvent.AddDynamic(this, &YourClass::YourMethod);
'

The Likert Scale will emit events of the following types:
- AnswerSelected (User clicked on a certain answer of current question)
- Submit (All questions answered and usere clicked on submit)
- Previous (User Went back to the previous Question)
- Next (User Went to the next question)
- CustomButton (User clicked on the custem button of the current question)

Each event contains the following propertys:
- Type (EEventType either AnswerSelected, Submit, Previous, Next, CustomButton)
- Questions (TArray<UQuestion*> Containing all questions of the Likert Scale)
- Answers (TArray<int16> Containing all given answers in Same order as "Questions" (NAN if Question not answered yet))
- TriggeredByQuestionIndex (int16 index of the question that the event was triggered by)



### Possible Configurations:
- Add Questions
- Modify Questions:
	- QuestionText
	- Agree and Disagree Texts
	- Answer Range
- Switch between multiple questions on the same widget and sequenced questions.
- Toggle Custom Button
- Enable going back and forth between question.


## Remarks

 - When creating a Likert Scale template the question name as defined in AddQuestion has to be a unique name.
